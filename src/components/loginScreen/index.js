import React, {useState} from 'react';
import {REGISTER} from '../../apicalls/LoginScreenCalls'
import Register from './Register';
import Login from './Login';


const LoginScreen = () => {
  const [userRegisterData, setUserRegisterData] = useState({ userName: '', password: '' });
  const [userLoginData, setUserLoginData] = useState({ userName: '', password: '' });
  const [loginScreenCase, setLoginScreenCase] = useState(false);

  const handleRegisterChange = e => {
    if(e.target.name === "name"){
        setUserRegisterData({...userRegisterData, userName: e.target.value})
    }
    else if(e.target.name === 'pwd') {
        setUserRegisterData({...userRegisterData, password: e.target.value})
    }
    else {
        console.log(e.target)
    }
  }

  const handleLoginChange = e => {
    if(e.target.name === "name"){
       setUserLoginData({...userLoginData, userName: e.target.value})
      }
      else if(e.target.name === 'pwd') {
        setUserLoginData({...userLoginData, password: e.target.value})
      }
      else {
          console.log(e.target)
      }
  }

  const handleRegister = e => {
    e.preventDefault()

    REGISTER(userRegisterData)
      .then(res => {
        setLog(res.message);
      }) 
  }

  const switchScreen = () => {
    setLoginScreenCase(!loginScreenCase);
  }

  if(!loginScreenCase){
    return (
      <Register handleRegister={handleRegister} handleChange= {handleRegisterChange} goLogin={switchScreen}/>
    );
  }
  else {
    return (
      <Login handleLogin={handleLogin} handleChange= {handleLoginChange} goRegister ={switchScreen} />
    );
  }
}

export default LoginScreen;