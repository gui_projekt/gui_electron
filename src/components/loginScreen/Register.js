import React from 'react'

const Register = props => {
  return (
    <div>
        <h1>Register</h1>
        <form onSubmit={props.handleRegister}>
            <label>Name</label><input type="text" name="name" value={props.userName} onChange={props.handleChange}></input>
            <label>Password</label><input name="pwd" type="password" value={props.userPassword} onChange={props.handleChange}></input>
            <button type="submit">Submit</button>
        </form>
            <a href="#" onClick={props.goLogin}>Go to Login Screen</a>
    </div>
  )
}

export default Register
