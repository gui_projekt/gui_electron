export const REGISTER = ({userName, password}) => {
return fetch('http://localhost:3000/users/register', {
     method: 'POST',
     headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
     body: JSON.stringify({
         userName,
         password
     })
 })
    .then(res => res.json())
    .catch(err => {
        console.log(err)
    })
}


export const LOGIN = token => {
return fetch('http://localhost:3000/users/login',{
        method: 'POST',
        headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
       },
        body: JSON.stringify({
            token
        })
    })
       .then(res => res.json())
       .catch(err => {
           console.log(err)
       })
    }